# graphql-client

- Author: Alexander Buck
- License: Apache 2

This is a Java GraphQL client library with a focus on providing a clean, simple
interface and having few dependencies.


Example:

        var query = "query($size:Int) { customers(size:$size) { name, location }}";
        var client = GraphQLClient.create(b -> b.uri(URI.create("https://www.example.com/graphql")));
        var resp = client.query(b -> b.query(query).variables(Map.of("size", 4)));
        var customers = resp.toList("customers", Customer.class);

