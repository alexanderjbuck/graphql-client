package com.alexanderjbuck.graphql;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Getter;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Builder
public class GraphQLResponse {
    private final ObjectMapper objectMapper;
    private final int statusCode;
    private final String body;

    public int statusCode() {
        return statusCode;
    }

    public String body() {
        return body;
    }

    @SneakyThrows
    public Optional<JsonNode> toTree() {
        if (statusCode != 200) {
            return Optional.empty();
        }
        return Optional.ofNullable(objectMapper.readTree(body));
    }

    public <T> Optional<T> toObject(String key, Class<T> clss) {
        if (statusCode != 200) {
            return Optional.empty();
        }
        return getDataNode(body, key).map(n -> {
            try {
                return objectMapper.treeToValue(n, clss);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public <T> List<T> toList(String key, Class<T> clss) {
        if (statusCode != 200) {
            return List.of();
        }
        var result = new ArrayList<T>();
        getDataNode(body, key).ifPresent(n -> {
                    try {
                        objectMapper.treeToValue(n, List.class).forEach(n2 -> result.add(objectMapper.convertValue(n2, clss)));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
        );
        return result;
    }

    @SneakyThrows
    private Optional<JsonNode> getDataNode(String json, String key) {
        var tree = objectMapper.readTree(json);
        var data = tree.get("data");
        if (data == null || data.isNull()) {
            return Optional.empty();
        }
        var node = data.get(key);
        if (node == null || node.isNull()) {
            return Optional.empty();
        }
        return Optional.of(node);
    }
}
