package com.alexanderjbuck.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import lombok.Builder;
import lombok.SneakyThrows;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.function.Consumer;

@Builder
public class GraphQLClient {
    private URI uri;
    @Builder.Default
    private HttpClient client = HttpClient.newHttpClient();
    @Builder.Default
    private ObjectMapper objectMapper =
            new ObjectMapper()
                    .registerModule(new ParameterNamesModule())
                    .registerModule(new Jdk8Module())
                    .registerModule(new JavaTimeModule());

    public static GraphQLClient create(Consumer<GraphQLClientBuilder> configure) {
        var builder = GraphQLClient.builder();
        configure.accept(builder);
        return builder.build();
    }

    /**
     * https://graphql.org/learn/serving-over-http/
     * @param requestBuilder
     * @return
     */
    @SneakyThrows
    public GraphQLResponse query(Consumer<GraphQLRequest.GraphQLRequestBuilder> requestBuilder) {
        var bodyNode = JsonNodeFactory.instance.objectNode();

        var builder = GraphQLRequest.builder();
        requestBuilder.accept(builder);
        var request = builder.build();

        bodyNode.put("query", request.getQuery());

        if (request.getOperation() != null) {
            bodyNode.put("operationName", request.getOperation());
        }

        if (request.getVariables() != null) {
            var variables = JsonNodeFactory.instance.objectNode();
            request.getVariables().entrySet().forEach(e -> variables.putPOJO(e.getKey(), e.getValue()));
            bodyNode.set("variables", variables);
        }

        var body = objectMapper.writeValueAsString(bodyNode);

        var req = HttpRequest.newBuilder(uri)
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();

        var resp = client.send(req, HttpResponse.BodyHandlers.ofString());

        return GraphQLResponse.builder()
                .objectMapper(objectMapper)
                .statusCode(resp.statusCode())
                .body(resp.body())
                .build();
    }

}
