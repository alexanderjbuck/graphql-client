package com.alexanderjbuck.graphql;

import lombok.Builder;
import lombok.Getter;

import java.util.Map;

@Getter
@Builder
public class GraphQLRequest {
    private String query;
    private String operation;
    private Map<String, Object> variables;
}
