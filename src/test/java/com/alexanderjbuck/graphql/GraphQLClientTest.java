package com.alexanderjbuck.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class GraphQLClientTest {
    @Data
    @NoArgsConstructor
    public static class Artist {
        String name;
        String birthday;
    }


    public static final String QUERY = "query($size:Int) {  artists(size:$size, sort:TRENDING_DESC) {    name,    birthday  }}";

    @Test
    public void error() throws Exception {
        var resp = GraphQLResponse.builder().statusCode(400).body("An error occurred").build();
        var artist = resp.toObject("artist", Artist.class);
        assertEquals(400, resp.statusCode());
        assertEquals("An error occurred", resp.body());
        assertFalse(artist.isPresent());
    }

    @Test
    public void data() throws Exception {
        var body = "{\"data\":{\"artist\":{\"name\":\"Pablo Picasso\",\"birthday\":\"1881\"}}}";
        var resp = GraphQLResponse.builder().body(body).objectMapper(new ObjectMapper()).statusCode(200).build();
        var artist = resp.toObject("artist", Artist.class).get();
        assertEquals("Pablo Picasso", artist.getName());
        assertEquals("1881", artist.getBirthday());
    }

    @Test
    public void listData() throws Exception {
        var body = "{\"data\":{\"artists\":[{\"name\":\"Pablo Picasso\",\"birthday\":\"1881\"},{\"name\":\"Raphael\",\"birthday\":\"1483\"}]}}";
        var resp = GraphQLResponse.builder().body(body).objectMapper(new ObjectMapper()).statusCode(200).build();
        var artists = resp.toList("artists", Artist.class);
        assertEquals(2, artists.size());
        assertEquals("Pablo Picasso", artists.get(0).getName());
        assertEquals("1881", artists.get(0).getBirthday());
        assertEquals("Raphael", artists.get(1).getName());
        assertEquals("1483", artists.get(1).getBirthday());
    }

    @Disabled
    @Test
    void liveTest() throws Exception {
        var client = GraphQLClient.create(b -> b.uri(URI.create("https://metaphysics-production.artsy.net")));
        var resp = client.query(b -> b.query(QUERY).variables(Map.of("size", 4)));

        assertEquals(200, resp.statusCode());

        var artists = resp.toList("artists", Artist.class);

        assertTrue(artists.size() <= 4);
        artists.stream().map(Artist::getName).forEach(System.out::println);
    }
}
